class CfgPatches {
	class ODIN {
		units[] = {""};
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Modules_F"};
	};
};

class CfgFactionClasses {
	class NO_CATEGORY;
	
	class odin_category_core: NO_CATEGORY {
		displayName = "ODIN - Core";
	};
	
	class odin_category_population: NO_CATEGORY {
		displayName = "ODIN - Population";
	};
	
};

class CfgVehicles {
	class Logic;
	class Module_F: Logic {
		class AttributesBase {
			class Default;
			class Edit;					// Default edit box (i.e., text input field)
			class Combo;				// Default combo box (i.e., drop-down menu)
			class Checkbox;				// Default checkbox (returned value is Boolean)
			class CheckboxNumber;		// Default checkbox (returned value is Number)
			class ModuleDescription;	// Module description
			class Units;				// Selection of units on which the module is applied
		};
		class ModuleDescription {
			class AnyPlayer;
			class AnyBrain;
			class Anything;
			class EmptyDetector;
		};
	};
	
	// =============================================
	// ODIN - CORE
	// =============================================
	class ODIN_ModuleInit: Module_F {
		scope = 2;
		icon = "\ODIN\assets\icons\init.paa";
		displayName = "[ODIN] Init";
		category = "odin_category_core";
		isGlobal = 2;
		functionPriority = 100;
		isTriggerActivated = 0;
		
		class Arguments: AttributesBase {
			class ODIN_Debug {
				displayName = "Debug";
				property = "odin_debug";
				description = "Debug mode.";
				typeName = "BOOL";
				defaultValue = false;
			};
		};
		
		class Eventhandlers {
			init = "_this execVM 'ODIN\scripts\modules\core\fn_ModuleInit.sqf';";
		};
	};
	
	// =============================================
	// ODIN - POPULATION
	// =============================================
	class ODIN_ModuleCreateZone: Module_F {
		scope = 2;
		icon = "\ODIN\assets\icons\zone.paa";
		displayName = "[ODIN] Create Zone";
		category = "odin_category_population";
		isGlobal = 2;
		functionPriority = 0;
		isTriggerActivated = 0;
		
		class Arguments: AttributesBase {
			
			class ODIN_AreaType {
				displayName = "Area Type";
				property = "odin_areatype";
				description = "Set the type of area that this objective is";
				typeName = "NUMBER";
				defaultValue = 0;
				class values {
					class pth_junc	{name = "Pathfinding Junction";	value = -1;};
					class civ_town	{name = "Civ. Town";			value = 0;};
					class civ_air	{name = "Civ. Airport";			value = 1;};
					class civ_naval	{name = "Civ. Harbor";			value = 2;};
					class mil_base 	{name = "Mil. Base";			value = 3;};
					class mil_post	{name = "Mil. Outpost";			value = 4;};
					class mil_air	{name = "Mil. Airbase";			value = 5;};
					class mil_naval	{name = "Mil. Harbor";			value = 6;};
					class inf_power {name = "Inf: Power";			value = 7;};
					class inf_fuel	{name = "Inf: Oil/Fuel";		value = 8;};
					class inf_rail	{name = "Inf: Railway";			value = 9;};
					class inf_comms {name = "Inf: Communications";	value = 10;};
				};
			};
			class ODIN_AreaSize {
				displayName = "Area Size";
				property = "odin_areasize";
				description = "Set the size of the area. Given measurements are the radius from the center of this module.";
				typeName = "NUMBER";
				defaultValue = 250;
				class values {
					class none		{name = "None (0m)";			value = 0;};
					class mini		{name = "Miniscule (50m)";		value = 50;};
					class small		{name = "Small (100m)";			value = 100;};
					class medium	{name = "Medium (250m)";		value = 250;};
					class large		{name = "Large (500m)";			value = 500;};
					class massive	{name = "Massive (1km)";		value = 1000;};
				};
			};
			class ODIN_AreaPriority {
				displayName = "Area Priority";
				property = "odin_areasize";
				description = "Set the priority of this area to the AI commander.";
				typeName = "NUMBER";
				defaultValue = 50;
				class values {
					class mini		{name = "Fodder";		value = 0;};
					class low		{name = "Low";			value = 25;};
					class medium	{name = "Medium";		value = 50;};
					class high		{name = "High";			value = 75;};
					class critical	{name = "Critical";		value = 100;};
				};
			};
			class ODIN_StartPopulated {
				displayName = "Start Populated";
				property = "odin_startpopulated";
				description = "Have this area populated at mission start."
				typeName = "BOOL";
				defaultValue = true;
			};
			
			class ModuleDescription: ModuleDescription{};
		};
		
		class ModuleDescription: ModuleDescription {
			description = "Create an ODIN Zone for the OPFOR commander to use.";
			sync[] = {"Anything"};
			class AnyVehicle {
				description = "Any object - persons, vehicles, static objects, etc.";
				displayName = "Anything";
				side = 4;
				position = 1;
				direction = 0;
				optional = 1;
				duplicate = 1;
			};
		};
		
		class Eventhandlers {
			init = "_this execVM 'ODIN\scripts\modules\population\fn_ModuleCreateZone.sqf';";
		};
		
	};
	
	class ODIN_OpforCommander: Module_F {
		scope = 2;
		icon = "\ODIN\assets\icons\opfor.paa";
		displayName = "[ODIN] Opfor Commander";
		category = "odin_category_population";
		isGlobal = 2;
		functionPriority = 0;
		isTriggerActivated = 0;
		
		class Arguments: AttributesBase {
			
			class ODIN_Side {
				displayName = "Force Side";
				property = "odin_opforside";
				description = "The side that your enemy force is a part of";
				typeName = "NUMBER";
				defaultValue = 1;
				class values {
					class blu	{name = "BLUFOR"; value = 0;};
					class red	{name = "OPFOR"; value = 1;};
					class grn	{name = "GREENFOR"; value = 2;};
//					class civ	{name = "Civilian"; value = 3;};
				};
			};
			
			class ODIN_Classname {
				displayName = "Force Classname";
				property = "odin_classname";
				description = "The classname of OPFOR forces. Check CfgGroups for this.";
				typeName = "STRING";
				defaultValue = "OPF_F";
			};
		};
		class Eventhandlers {
			init = "_this execVM 'ODIN\scripts\modules\population\fn_ModuleOpforCommander.sqf';";
		};
	};
	
	
	class ODIN_Route: Module_F {
		scope = 2;
		icon = "\ODIN\assets\icons\route.paa";
		displayName = "[ODIN] Route";
		category = "odin_category_population";
		isGlobal = 2;
		functionPriority = 0;
		isTriggerActivated = 0;
		
		class Arguments: AttributesBase {
			class ODIN_TransportationType {
				displayName = "Transportation Type";
				property = "odin_transporttype";
				description = "Override the default long-range transportation type of enemy units";
				typeName = "NUMBER";
				defaultValue = 0;
				class values {
					class ns	{name = "Don't Override";		value = 0;};
					class sea	{name = "Sea"; 					value = 1;};
					class air	{name = "Air"; 					value = 2;};
					class vic	{name = "Motorized/Mechanized";	value = 3;};
					class foot	{name = "Foot";					value = 4;};
				};
			};
		};
		
		class Eventhandlers {
			init = "_this execVM 'ODIN\scripts\modules\population\fn_ModuleRoute.sqf';";
		};
	};
	
	class ODIN_OpforBase: Module_F {
		scope = 2;
		icon = "\ODIN\assets\icons\opforbase.paa";
		displayName = "[ODIN] OPFOR Base";
		category = "odin_category_population";
		isGlobal = 2;
		functionPriority = 0;
		isTriggerActivated = 0;

		class Eventhandlers {
			init = "_this execVM 'ODIN\scripts\modules\population\fn_ModuleRoute.sqf';";
		};
	};
};



class CfgFunctions {
	class ODIN {
/*
		class odin_category_core {
			file = "\ODIN\scripts\modules\core";
			class ModuleInit{};
		};
		
		class odin_category_population {
			file = "\ODIN\scripts\modules\population";
			class ModuleCreateZone{};
			class ModuleOpforCommander{};
		};
*/
		class General {
			file = "\ODIN\scripts\general";
			class CheckInitPlaced {};
			class GetVariable{};
		};
	};
};
