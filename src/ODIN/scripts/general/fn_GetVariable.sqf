/**
Gets a variable form the ODIN init module.

*/
params ["_key", "_defaultValue"];

_init = missionNameSpace getVariable "ODIN_INITModule";
_init getVariable [_key, _defaultValue]