/**
This file checks to see if the init module has placed itself inside the missionnamespace.

RETURNS:
	true: The module was found and we're all good.
	false: The module wasn't found.

*/

// Wait until Arma is done doing its thing.
waitUntil { !isNull (missionnamespace getVariable ["ODIN_INITMODULE", objNull]) };

// If the init module couldn't place itself in the namespace then we've got a problem.
if( isNull (missionNameSpace getVariable ["ODIN_INITMODULE", objNull])) then {
	hint "The ODIN Init module wasn't placed! ODIN won't work!"
	exitWith { false };
};

true