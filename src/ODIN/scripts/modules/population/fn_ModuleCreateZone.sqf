if(is3den) exitwith {false};
/**
This module handles all of the logic needed to create a "zone" in odin.


*/

waitUntil {(call ODIN_fnc_CheckInitPlaced)};

_logic = param [0,objNull,[objNull]];
_synced = synchronizedObjects _logic;
_init = missionNameSpace getVariable "odin_initmodule";



//DEBUG FUNCTIONS: Create a marker on the position with some data about the zone.
if(_init getVariable ["odin_debug", false]) then {
	
	diag_log (["Module ", _logic, " executed!"] joinString "");
	
	//Debug colorscheme for areas
	_color = "";
	switch (_logic getVariable "odin_areaPriority") do {
	case 0:		{_color = "ColorWhite"; };
	case 25:	{_color = "ColorGreen";	};
	case 50:	{_color = "ColorYellow";};
	case 75:	{_color = "ColorOrange";};
	case 100:	{_color = "ColorRed";};
	};

	// Place a marker on the area.
	_size = _logic getvariable ["odin_areasize", 50];
	[["area_", _logic] joinString "", position _logic, "ellipse", [_size * 2, _size * 2], "COLOR:", _color, "PERSIST"] spawn cba_fnc_createmarker;
	
	[["desc_", _logic] joinString "", position _logic, "ICON", [0, 0], "TEXT:", ["RAD:", _logic getvariable "odin_areasize", "TYP:", _logic getVariable "odin_areatype", "PRI:", _logic getVariable "odin_areapriority"] joinString " ", "COLOR:", "ColorBlack", "TYPE:", "hd_objective", "PERSIST"] spawn cba_fnc_createmarker;
};

//Add this module to the list of zones...
_allzones = _init getVariable "odin_zones";
_allzones pushBack _logic;	//cant one-line this because this returns the index.
_init setVariable ["odin_zones", _allzones];


//Separate everything but other zones from the list of modules.
_special = [];
_zones = [];
{
	if (typeOf _x == "ODIN_ModuleCreateZone") then {
		_zones pushBack _x;
	} else {
		_special pushBack _x;
	};
} forEach (_synced);

// And add a list of the connected zones/specials to a variable in this module's namespace.
_logic setVariable ["odin_connected", _zones];
_logic setVariable ["odin_special", _special];

// Now let's populate the area.

true